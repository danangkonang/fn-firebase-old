import firebase from 'react-native-firebase'
import type { Notification,NotificationOpen} from 'react-native-firebase'

class FCmservice {

   register = (onRegister,onNotification,onOpenNotification)=>{
      this.checkPermision(onRegister)
      this.createNotification(onNotification,onOpenNotification)
   }

   checkPermision = (onRegister)=>{
      // console.log("testing",onRegister)
      firebase.messaging().hasPermission()
      .then(enable=>{
         if(enable){
            this.getToken(onRegister)
            console.log("permision true",enable)
         }else{
            this.requestPermision(onRegister)
            console.log("permision false",enable)
         }
      }).catch(err=>{
         console.log("promise rejected",err)
      })
   }

   getToken = (onRegister)=>{
      firebase.messaging().getToken()
      .then(fcmToken=>{
         if(fcmToken){
            onRegister(fcmToken)
         }else{
            console.log("user don not token")
         }
      }).catch(err=>{
         console.log("get token rejected",err)
      })
   }

   requestPermision = (onRegister)=>{
      firebase.messaging().requestPermission()
      .then(()=>{
         this.getToken(onRegister)
      }).catch(err=>{
         console.log("request permision rejected",err)
      })
   }

   deleteToken = ()=>{
      firebase.messaging().deleteToken()
      .catch(err=>{
         console.log("delete token error",err)
      })
   }
   createNotification = (onNotification,onOpenNotification)=>{
      this.notificationListener = firebase.notifications().onNotification((notification:Notification)=>{
         onNotification(notification)
      })

      this.notificationOpenedListener = firebase.notifications()
      .onNotificationOpened((notificationOpen:NotificationOpen)=>{
         onOpenNotification(notificationOpen)
      })

      firebase.notifications().getInitialNotification()
      .then(notificationOpen=>{
         if(notificationOpen){
            const notification:Notification = notificationOpen.notification
            onOpenNotification(notification)
         }
      })

      this.messageListener = firebase.messaging().onMessage(message=>{
         onNotification(message)
      })

      this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken=>{
         console.log("new token",fcmToken)
         onRegister(fcmToken)
      })

   }

   unRegister = ()=>{
      this.notificationListener()
      this.notificationOpenedListener()
      this.messageListener()
      this.onTokenRefreshListener()
   }

   buildChannel = (obj)=>{
      return new firebase.notifications.Android.Channel(
         obj.channelId,obj.channelName,
         firebase.notifications.Android.Importance.High)
         .setDescription(obj.channelDe)
   }
   
   buildNotification = (obj)=>{
      firebase.notifications().android.createChannel(obj.channel)

      return new firebase.notifications.Notification()
      .setSound(obj.sound)
      .setNotificationId(obj.dataId)
      .setTitle(obj.title)
      .setBody(obj.content)
      .setData(obj.data)

      .android.setChannelId(obj.channel.channelId)
      // .android.setLargeIcon(obj.largeIcon)
      // .android.setSmallIcon(obj.smallIcon)
      .android.setColor(obj.colorBgIcon)
      .android.setPriority(firebase.notifications.Android.Priority.High)
      .android.setVibrate(obj.vibrate)
      // .android.setAutoCancel(true)
   }

   scheduledNotification = (notification,days,minutes)=>{
      const date = new Date()
      if(days){
         date.setDate(date.getDate()+days)
      }
      if(minutes){
         date.setMinutes(date.getMinutes()+minutes)
      }
      firebase.notifications()
      .scheduleNotification(notification,{fireDate:date.getTime()})
   }

   displayNotification = (notification)=>{
      firebase.notifications().displayNotification(notification)
      .catch(err=>console.log("display nya error mas brow",err))
   }

   removeDelivereNotivication = (notification)=>{
      firebase.notifications()
      .removeAllDeliveredNotifications(notification.notificationId)
   }
}

export const fcmService = new FCmservice()