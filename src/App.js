import React, { Component } from 'react'
import { 
  View,
  Text,
} from 'react-native'
import {fcmService} from './FCMservice'
export default class App extends Component {
   constructor(props){
      super(props)
   }

   componentDidMount(){
      fcmService.register(this.onRegister, this.onNotification,this.onOpenNotification)
   }

   onRegister(token){
      console.log("onregister", token)
   }

   onNotification(notify){
      const channelObj = {
         channelId:"sample",
         channelName:"name",
         channelDes:"deskrip"
      }
      const channel = fcmService.buildChannel(channelObj)

      const buildNotify = {
         dataId:notify._notificationId,
         title:notify._title,
         content:notify._body,
         sound:"default",
         channel:channel,
         data:{},
         colorBgIcon:"#aaaaaa",
         largeIcon:"icon_notife",
         smallIcon:"icon_notife",
         vibrate:true,
      }
      const notification = fcmService.buildNotification(buildNotify)
      console.log("ini notif nya",notification)

      fcmService.displayNotification(notification)
   }

   onOpenNotification(notif){
      console.log("open notif",notif)
   }

   render(){
      return(
         <View style={{flex:1,backgroundColor:'tomato'}}>
            <Text>testing</Text>
         </View>
      )
   }
}